<?php
$lang->pipeline->id       = 'ID';
$lang->pipeline->name     = '服务器名称';
$lang->pipeline->url      = '服务器地址';
$lang->pipeline->token    = 'Token';
$lang->pipeline->account  = '用户名';
$lang->pipeline->password = '密码';
